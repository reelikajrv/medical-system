import os
import sqlite3

from flask  import g

def init_db(db_name):
    open(os.path.join('database', db_name + '.sqlitedb'), 'w').close()
    db = sqlite3.connect(os.path.join('database', db_name + '.sqlitedb'))
    schemas_dir = os.path.join('database', 'schema')
    schema_found = db_name + '.sql' in os.listdir(schemas_dir)
    if schema_found:
        with open(os.path.join(schemas_dir, db_name + '.sql'), 'r') as schema:
            db.executescript(schema.read())
    db.commit()
    db.close()
    print('Database `%s` initialized %s sql-schema'
          %(db_name, ['WITHOUT', 'WITH'][schema_found]))
    return

def get_db(db_name):
    if not getattr(g, 'db_' + db_name, False):
        uri = os.path.join('database', db_name + '.sqlitedb')
        #db  = sqlite3.connect('%s?mode=%s' %(uri, mode), uri=True)
        db = sqlite3.connect(uri)
        setattr(g, 'db_' + db_name, db)
    return db

def close_all_db():
    for db_name in filter(lambda x: x.startswith('db_'), dir(g)):
        db = getattr(g, db_name)
        db.commit()
        db.close()
    return
