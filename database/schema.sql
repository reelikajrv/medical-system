CREATE TABLE `patients` (
    `code`  TEXT UNIQUE,
    `name`  TEXT,
    `age`   TEXT,
    `gender`    TEXT,
    `flags` TEXT,
    `stage` INTEGER,
    PRIMARY KEY(`code`)
);

CREATE TABLE `reception_entry` (
    `code`  TEXT UNIQUE,
    `date`  TEXT,
    `timestamp` INTEGER
);

CREATE TABLE `blood_in` (
    `sheet_code`    TEXT,
    `lab_code`  TEXT,
    `name`  TEXT,
    `hasleft`   INTEGER,
    `date`  TEXT,
    `timestamp` INTEGER
);

CREATE TABLE `blood_out` (
    `code`  TEXT,
    `date`  TEXT,
    `timestamp` INTEGER,
    `urea`  TEXT,
    `creatinine`    TEXT,
    `uric`  TEXT,
    `albumin`   TEXT,
    `bilirubin_direct`  TEXT,
    `bilirubin_total`   TEXT,
    `glucose`   TEXT,
    `pt`    TEXT,
    `asot`  TEXT,
    `cbc`   TEXT,
    `gpt`   TEXT,
    `crp`   TEXT,
    `hepatitis` TEXT,
    `esr`   TEXT
);

CREATE TABLE `urine_in` (
    `sheet_code`    TEXT,
    `lab_code`  TEXT,
    `name`  TEXT,
    `hasleft`   INTEGER,
    `date`  TEXT,
    `timestamp` INTEGER
);

CREATE TABLE `urine_out` (
    `code`  TEXT,
    `date`  TEXT,
    `timestamp` INTEGER,
    `rbc`   TEXT,
    `pus`   TEXT,
    `epith` TEXT,
    `cast`  TEXT,
    `ca_ox` TEXT,
    `phosph`    TEXT,
    `uric`  TEXT,
    `amorph`    TEXT,
    `other` TEXT
);

CREATE TABLE `stool_in` (
    `sheet_code`    TEXT,
    `lab_code`  TEXT,
    `name`  TEXT,
    `hasleft`   INTEGER,
    `date`  TEXT,
    `timestamp` INTEGER
);

CREATE TABLE `stool_out` (
    `code`  TEXT,
    `date`  TEXT,
    `timestamp` INTEGER,
    `ehistolytica`  TEXT,
    `ecoli` TEXT,
    `glamblia`  TEXT,
    `enterobius`    TEXT,
    `hnana` TEXT,
    `taenia`    TEXT,
    `ancylostoma`   TEXT,
    `fasciola`  TEXT,
    `ascaris`   TEXT,
    `schmansoni`    TEXT,
    `other` TEXT
);

CREATE TABLE `pharmacy_out` (
    `code`  TEXT UNIQUE,
    `diagnosis` TEXT,
    `drugs` TEXT,
    `date`  TEXT,
    `timestamp` INTEGER
);

CREATE TABLE `dispensed_drugs` (
    `drug`  TEXT,
    `qty`   INTEGER,
    `date`  INTEGER,
    `timestamp` INTEGER
);
