// Sidebar
var sidebar_is_shown = false;
function toggleSidebar() {
    if (sidebar_is_shown) {
        sidebar_is_shown = false;
        $('div.sidebar').animate({marginLeft: -210}, 300);
    } else {
        sidebar_is_shown = true;
        $('div.sidebar').animate({marginLeft: 0}, 300);
    }
}
$('button.navbar-toggle').click(function() { toggleSidebar() });

// Pages
$('.sidebar a[data-target-page]').click(function() {
    if ($(this).parent().hasClass('disabled'))
        return;
    $('page.active').removeClass('active');
    $('.sidebar li.active').removeClass('active');
    $('page#' + $(this).data('target-page')).addClass('active');
    $(this).parent().addClass('active');
    toggleSidebar();
    $('page#' + $(this).data('target-page') + 'section.active').trigger('load');
});

// Sections
$('page .page-header ul li').click(function() {
   if ($(this).hasClass('disabled'))
      return;
   var page = $(this).parent().parent().parent();
   $('section.active', page).removeClass('active');
   $('.page-header ul li', page).removeClass('active');
   $('section#' + $(this).data('target-section'), page).addClass('active');
   $(this).addClass('active');
   $('.page-header span', page).html('- ' + $(this).html());
   $('section#' + $(this).data('target-section'), page).trigger('load');
})

// Forms
jQuery.fn.extend({
   disable: function(state) {
      return this.each(function() {
      this.disabled = state;
     });
   }
});

function clearForm(form) {
   $('input[type="text"], input[type="number"], select', form).val('');
   $('input[type="radio"]', form).prop('checked', false);
   $('input[type="checkbox"]', form).prop('checked', false);
   $('textarea', form).val('');
//   if ($(form, 'page#pharmacy section#patients'))
//      $('page#pharmacy section#patients tbody').html('<tbody><tr><td><input type="text" name="drugs-0" autocomplete="off"></td><td><input type="number" name="qty-0" autocomplete="off"></td></tr></tbody>');
   return;
}

$("form").submit(function(e){
   var form = $(this);
   var request = {
         url   : form.attr('action'),
         type  : form.attr('method'),
         data  : form.serialize(),
         success: function(response) {
            $('.feedback', form).html(response);
         },
         error: function() {
            $('.feedback', form).html('<span class="error">Unexpected error or server is down</span>');
         },
         complete: function(jqXHR, textStatus) {
            clearForm(form);
            $('input, textarea, select, button', form).disable(false);
            $('input', form).eq(0).focus();

            if ($('page#pharmacy').hasClass('active') && $('section#patients').hasClass('active'))
               $('page#pharmacy section#patients input[name="action"]').val('insert');
               $('page#pharmacy section#patients tbody').html('<tr><td><input type="text" name="drugs-0" autocomplete="off"></td><td><input type="number" name="qty-0" autocomplete="off"></td><td><button type="button" class="btn btn-success btn-sm" onclick="dispense(this)"><i class="fa fa-check"></i></button></td></tr>');
               $('page#pharmacy section#patients table input').on('input', pharmacyCheckDrugs);
               $('#ph_patients_feedback').html('');
               $('input#ph_code').focus();
         }
    }
   $('.feedback', form).html('Working...');
   $('input, textarea, select, button', form).disable(true);
   $.ajax(request);
   return false;
 });

// Download
function download(filename, text) {
    var pom = document.createElement('a');
    pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    pom.setAttribute('download', filename);

    if (document.createEvent) {
        var event = document.createEvent('MouseEvents');
        event.initEvent('click', true, true);
        pom.dispatchEvent(event);
    }
    else {
        pom.click();
    }
}

// LABS NAMES
names_in_blood = [];
names_in_urine = [];
names_in_stool = [];
drug_names = [];

function getBloodNames() {
   $.get('/blood/names', '', function(data) {
      names_in_blood = JSON.parse(unescape(data));
   });
}

function getUrineNames() {
   $.get('/urine/names', '', function(data) {
      names_in_urine = JSON.parse(unescape(data));
   });
}

function getStoolNames() {
   $.get('/stool/names', '', function(data) {
      names_in_urine = JSON.parse(unescape(data));
   });
}

function getDrugNames() {
   $.get('/pharmacy/drugnames', '', function(data) {
      drug_names = JSON.parse(unescape(data));
   });
}

getBloodNames();
getUrineNames();
getStoolNames();
getDrugNames();

setInterval(function(){getBloodNames();}, 3 * 60 * 1000);
setInterval(function(){getUrineNames();}, 3 * 60 * 1000);
setInterval(function(){getStoolNames();}, 3 * 60 * 1000);
setInterval(function(){getDrugNames();}, 3 * 60 * 1000);

// HELP
function help() {
   message = prompt('Leave a message to system admin');
   if (!message)
      return
   $.ajax({
         url   : '/msg-admin',
         type  : 'POST',
         data  : {message: message},
         success: function(response) {
            alert(response);
         },
         error: function() {
            alert('Unknown error');
         }
    });
}
$('.navbar i#help').click(function() {help();});

// PHARMACY DRUGS
function pharmacyCheckDrugs() {
   var input_left_empty = false;
   $('page#pharmacy section#patients input[name^="drug"]').each(function() {
      if (!($(this).val()))
         input_left_empty = true;
   })
   if (!input_left_empty) {
      var i = $('input[name^="drug"]').length;
      $('page#pharmacy section#patients table tbody').append('<tr><td><input type="text" name="drugs-' +i+ '" autocomplete="off"></td><td><input type="number" name="qty-' +i+ '" autocomplete="off"></td><td><button type="button" class="btn btn-success btn-sm" onclick="dispense(this)"><i class="fa fa-check"></i></button></td></tr>');
      $('page#pharmacy section#patients table input').on('input', pharmacyCheckDrugs);
//      $('input[data-autocomplete-using]').on('input', autocomplete);
   }
   return;
}
$('page#pharmacy section#patients table input').on('input', pharmacyCheckDrugs);

// AUTOCOMPLETE
//dum = ['cat', 'car', 'caffe'];
//function autocomplete() {
//   var uselist = window[$(this).data('autocomplete-using')];
//
//   return;
//}
//
//function autocompleteUp(e) {
//   return;
//}
//
//function autocompleteDown(e) {
//   return;
//}
//
//var ARROW_UP = 38;
//var ARROW_DOWN = 40;
//$('input[data-autocomplete-using]').on('input', autocomplete);


$('#rece_entry_code').keydown(function(e) {
    if (e.keyCode >= 65 && e.keyCode <= 90 && !e.altKey && !e.ctrlKey) {
        x = e.keyCode  - 65;
        l = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
        $('#rece_entry_code').val($('#rece_entry_code').val() + l[x]);
        return false;
    }
});


function check() {
   $('#ph_patients_feedback').html('');
   if (!$('#ph_code').val())
      return
   $.ajax({
         url   : '/pharmacy/load_patient',
         type  : 'GET',
         data  : {code: $('#ph_code').val()},
         success: function(response) {
            if (response) {
               data = JSON.parse(unescape(response));
               if ('message' in data) {
                  console.log(data);
                  $('#ph_patients_feedback').html(data['message']);
                  return;
               }
               $('#diag').val(data.diag);
               data.drugs.forEach(function(e) {
                  $('[name^=drug]').last().val(e[0]);
                  $('[name^=qty]').last().val(e[1]);
                  pharmacyCheckDrugs();
               });
               $('page#pharmacy section#patients input[name="action"]').val('update');
            }
         },
         error: function() {
            return;
         }
    });
}
