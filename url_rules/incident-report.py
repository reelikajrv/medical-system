from flask  import *

import os

report_template = '''\
INCIDENT REPORT @ %s

NAME: %s
TEAM: %s


INCIDENT SUMMARY:
%s


SUGGESTED SOLUTION:
%s
'''

def post():
   global report_template
   try:
      name = request.form['name']
      team = request.form['team']
      details = request.form['details']
      solution = request.form.get('solution', '')

      filename = os.path.join('incident-reports', str(lib.time()) + '.txt')
      with open(filename, 'w', encoding='utf-8') as file:
         file.write(report_template %(str(lib.datetime()), name, team, details, solution))
      return '<span class="success">Done.</span>'
   except Exception as error:
      return '<span class="error">Server Error: %s</span>' %error
