from flask  import *

def get():
    return render_template('login.html')

def post():
    if 'username' not in request.form:
        return redirect(url_for('/login'))
    session['username'] = request.form['username']
    session['loggedin'] = True
    return redirect(url_for('/'))
