from flask  import *

def post():
   try:
      db = get_db('database')
      lab_code = request.form['lab_code'].lower().strip()

      rbcs = request.form['rbcs']
      pus = request.form['pus']
      epith = request.form['epith']
      cast = request.form['cast']
      ca_ox = request.form['ca_ox']
      phosph = request.form['phosph']
      uric = request.form['uric']
      amorph = request.form['amorph']
      other = request.form['other']

      code = db.execute('SELECT sheet_code FROM urine_in WHERE lab_code=? AND hasleft!=1', (lab_code,)).fetchall()
      if not code:
         return '<span class="error">Patient name doesn\'t exist in urine lab</span>'
      code=code[0][0]

      db.execute('INSERT INTO urine_out (code, date, timestamp, rbc, pus, epith, `cast`, ca_ox, phosph, uric, amorph, other) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)', (
         code,
         lib.date(),
         lib.time(),
         rbcs,
         pus,
         epith,
         cast,
         ca_ox,
         phosph,
         uric,
         amorph,
         other
         ))
      db.execute('UPDATE urine_in SET hasleft=1 WHERE sheet_code=?', (code,))

      in_stool = bool(db.execute('SELECT * FROM stool_in WHERE sheet_code=? AND hasleft!=1', (code,)).fetchall())
      in_blood = bool(db.execute('SELECT * FROM blood_in WHERE sheet_code=? AND hasleft!=1', (code,)).fetchall())
      r = '<span class="success">Done.'
      if not in_stool and not in_blood:
         return r + ' Patient is not in any other labs</span>'
      if in_stool and not in_blood:
         return r + ' Patient is still in stool lab!</span>'
      if in_blood and not in_stool:
         return r + ' Patient is still in blood lab!</span>'
      if in_stool and in_blood:
         return r + ' Patient is still in stool and blood labs!</span>'

   except Exception as error:
      return '<span class="error">Server Error: %s</span>' %error
