from flask  import *

def post():
   try:
      sheet_code = request.form['sheet_code'].lower().strip()
      lab_code = request.form['lab_code'].lower().strip()
      db = get_db('database')
      name = db.execute('SELECT name FROM patients WHERE code=?', (sheet_code,)).fetchall()
      if not name:
         name='[undefined]'
         patient_in_recep = False
         #return '<span class="error">ERROR: Patient code did not go through reception</span>'
      else:
         name = name[0][0]
         patient_in_recep = True
      db.execute('INSERT INTO urine_in (sheet_code, lab_code, date, timestamp, name, hasleft) VALUES (?, ?, ?, ?, ?, ?)', (
         sheet_code,
         lab_code,
         lib.date(),
         lib.time(),
         name,
         0
         ))
      db.execute('UPDATE patients SET stage=2 WHERE code=?', (sheet_code,))
      r = '<span class="success">Done.</span>'
      if not patient_in_recep:
         r += ' <span class="warning"> Warning: Patient is not in reception.</span>'
      return r
   except Exception as error:
      return '<span class="error">Server Error: %s</span>' %error
