from flask  import *

import os

message_template = '''\
Message to Admin @ %s
By: %s

%s
'''

def post():
    try:
        global message_template
        message = request.form['message']
        filename = os.path.join('admin-messages', str(lib.time()) + '.txt')
        with open(filename, 'w', encoding='utf-8') as file:
            file.write(message_template %(str(lib.datetime()), session['username'], message))
        return 'Message sent!'
    except:
        return 'Server error.'

