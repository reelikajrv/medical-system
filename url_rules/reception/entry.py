from flask  import *

def post():
    try:
        db = get_db('database')
        code = request.form['code'].strip().lower()
        name = request.form['name']
        occupation = request.form['occupation']
        age = request.form['age']
        gender = request.form['gender']
        height = request.form['height']
        weight = request.form['weight']
        bp = request.form['blood_pressure']
        bs = request.form['blood_sugar']
        history = request.form['history']
        flags = ""
        if 'smoker' in request.form:
            flags += 'smoker|'
        if 'hypertension' in request.form:
            flags += 'hypertensive|'
        if 'diabetes' in request.form:
            flags += 'diabetic|'
        flags = flags[:-1]
        stage = 1
        db.execute('INSERT INTO patients (code, name, occupation, age, gender, height, weight, blood_pressure, blood_sugar, history, flags, stage) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', (
            code,
            name,
            occupation,
            age,
            gender,
            height,
            weight,
            bp,
            bs,
            history,
            flags,
            stage
        ))
        db.execute('INSERT INTO reception_entry (code, date, timestamp) VALUES (?, ?, ?)', (
            code,
            lib.date(),
            lib.time()
        ))
        return '<span class="success">Done.</span>'
    except Exception as error:
        return '<span class="error">Server Error: %s</span>' %error
