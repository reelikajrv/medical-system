from flask  import *

def post():
    try:
        db = get_db('database')
        code = request.form['code'].strip().lower()
        name = request.form['name'].strip()
        if code and not name:
            test = db.execute('SELECT * FROM patients WHERE code=?', (code,)).fetchall()
            if not test:
                return '<span class="error">ERROR: Code did not go through reception</span>'
            test = db.execute('SELECT * FROM patients WHERE code=? and stage=1', (code,)).fetchall()
            if not test:
                return '<span class="error">ERROR: Patient already went to labs/pharmacy</span>'
            db.execute('DELETE FROM patients WHERE code=?', (code,))
            db.execute('DELETE FROM reception_entry WHERE code=?', (code,))
            return '<span class="success">Done.</span>'
        elif name and not code:
            test = db.execute('SELECT * FROM patients WHERE name=?', (name,)).fetchall()
            if not test:
                return '<span class="error">ERROR: Name did not go through reception</span>'
            test = db.execute('SELECT code FROM patients WHERE name=? and stage=1', (name,)).fetchall()
            if not test:
                return '<span class="error">ERROR: Patient already went to labs/pharmacy</span>'
            code = test[0][0]
            db.execute('DELETE FROM patients WHERE code=?', (code,))
            db.execute('DELETE FROM reception_entry WHERE code=?', (code,))
            return '<span class="success">Done.</span>'
        else:
            return '<span class="error">ERROR: Enter either code only or name only</span>'
    except Exception as error:
        return '<span class="error">Server Error: %s</span>' %error
