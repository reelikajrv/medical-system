from flask  import *

import json

def get():
    data = []
    db = get_db('database')
    for row in db.execute('SELECT patients.code, patients.name, reception_entry.timestamp, patients.gender, patients.age, patients.stage FROM patients INNER JOIN reception_entry ON patients.code=reception_entry.code ORDER BY reception_entry.timestamp').fetchall():
        row = list(row)
        row[2] = lib.str_time(row[2])
        data.append(row)
    return json.dumps(data)
