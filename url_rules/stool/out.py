from flask  import *

def post():
   try:
      db = get_db('database')
      lab_code = request.form['lab_code'].lower().strip()

      ehistolytica = request.form['ehistolytica']
      ecoli = request.form['ecoli']
      glamblia = request.form['glamblia']
      enterobius = request.form['enterobius']
      hnana = request.form['hnana']
      taenia = request.form['taenia']
      ancylostoma = request.form['ancylostoma']
      fasciola = request.form['fasciola']
      ascaris = request.form['ascaris']
      schmansoni = request.form['schmansoni']
      other = request.form['other']

      code = db.execute('SELECT sheet_code FROM stool_in WHERE lab_code=? AND hasleft!=1', (lab_code,)).fetchall()
      if not code:
         return '<span class="error">Patient name doesn\'t exist in stool lab</span>'
      sheet_code=code[0][0]

      db.execute('INSERT INTO stool_out (code, date, timestamp, ehistolytica, ecoli, glamblia, enterobius, hnana, taenia, ancylostoma, fasciola, ascaris, schmansoni, other) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)', (
         sheet_code,
         lib.date(),
         lib.time(),
         ehistolytica,
         ecoli,
         glamblia,
         enterobius,
         hnana,
         taenia,
         ancylostoma,
         fasciola,
         ascaris,
         schmansoni,
         other
         ))
      db.execute('UPDATE stool_in SET hasleft=1 WHERE sheet_code=?', (sheet_code,))

      in_urine = bool(db.execute('SELECT * FROM urine_in WHERE sheet_code=? AND hasleft!=1', (sheet_code,)).fetchall())
      in_blood = bool(db.execute('SELECT * FROM blood_in WHERE sheet_code=? AND hasleft!=1', (sheet_code,)).fetchall())
      r = '<span class="success">Done.'
      if not in_urine and not in_blood:
         return r + ' Patient is not in any other labs</span>'
      if in_urine and not in_blood:
         return r + ' Patient is still in urine lab!</span>'
      if in_blood and not in_urine:
         return r + ' Patient is still in blood lab!</span>'
      if in_urine and in_blood:
         return r + ' Patient is still in urine and blood labs!</span>'

      return '<span class="success">Done.</span>'

   except Exception as error:
      return '<span class="error">Server Error: %s</span>' %error
