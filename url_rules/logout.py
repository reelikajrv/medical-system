from flask  import *

def get():
    session.pop('username', None)
    session.pop('loggedin', None)
    return redirect(url_for('/login'))
