from flask  import *

def post():
   try:
      db = get_db('database')
      lab_code = request.form['lab_code'].lower().strip()
      
      urea = request.form['urea']
      crp = request.form['crp']
      asot = request.form['asot']
      creatinine = request.form['creatinine']
      cbc = request.form['cbc']
      hepatitis = request.form['hepatitis']
      pt = request.form['pt']
      glucose = request.form['glucose']
      gpt = request.form['gpt']
      bilirubin_direct = request.form['bilirubin_direct']
      bilirubin_total = request.form['bilirubin_total']
      uric = request.form['uric']
      albumin = request.form['albumin']
      esr = request.form['esr']
      
      code = db.execute('SELECT sheet_code FROM blood_in WHERE lab_code=? AND hasleft!=1', (lab_code,)).fetchall()
      if not code:
         return '<span class="error">Patient name doesn\'t exist in blood lab</span>'
      code=code[0][0]
      
      db.execute('INSERT INTO blood_out (code, date, timestamp, urea, crp, asot, creatinine, cbc, hepatitis, pt, glucose, gpt, bilirubin_direct, bilirubin_total, uric, albumin, esr) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', (
         code,
         lib.date(),
         lib.time(),
         urea,
         crp,
         asot,
         creatinine,
         cbc,
         hepatitis,
         pt,
         glucose,
         gpt,
         bilirubin_direct,
         bilirubin_total,
         uric,
         albumin,
         esr
         ))
      db.execute('UPDATE blood_in SET hasleft=1 WHERE sheet_code=?', (code,))

      in_urine = bool(db.execute('SELECT * FROM urine_in WHERE sheet_code=? AND hasleft!=1', (code,)).fetchall())
      in_stool = bool(db.execute('SELECT * FROM stool_in WHERE sheet_code=? AND hasleft!=1', (code,)).fetchall())
      r = '<span class="success">Done.'
      if not in_urine and not in_stool:
         return r + ' Patient is not in any other labs</span>'
      if in_urine and not in_stool:
         return r + ' Patient is still in urine lab!</span>'
      if in_stool and not in_urine:
         return r + ' Patient is still in stool lab!</span>'
      if in_urine and in_stool:
         return r + ' Patient is still in urine and stool labs!</span>'

   except Exception as error:
      return '<span class="error">Server Error: %s</span>' %error
