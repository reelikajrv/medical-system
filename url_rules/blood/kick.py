from flask  import *

def post():
    try:
        db = get_db('database')
        sheet_code = request.form['sheet_code'].strip().lower()
        lab_code = request.form['lab_code'].strip().lower()
        name = request.form['name'].strip()
        if not (sheet_code and lab_code and not name) or (name and not sheet_code and not lab_code):
            return '<span class="error">ERROR: Use two codes or name to kick</span>'
        if sheet_code and lab_code:
            test = db.execute('SELECT * FROM blood_in WHERE sheet_code=? and lab_code=? and hasleft!=1', (sheet_code, lab_code)).fetchall()
            if not test:
                return '<span class="error">ERROR: Sheet code not in blood lab</span>'
            db.execute('DELETE FROM blood_in WHERE sheet_code=?', (sheet_code,))
            return '<span class="success">Done.</span>'
        elif name:
            test = db.execute('SELECT * FROM blood_in WHERE name=? and hasleft!=1', (name,)).fetchall()
            if not test:
                return '<span class="error">ERROR: Name not in blood lab</span>'
            db.execute('DELETE FROM blood_in WHERE name=?', (name,))
            return '<span class="success">Done.</span>'
    except Exception as error:
        return '<span class="error">Server Error: %s</span>' %error
