from flask  import *

import json

def get():
    data = []
    db = get_db('database')
    for row in db.execute('SELECT sheet_code, lab_code, name, timestamp, hasleft FROM blood_in ORDER BY timestamp').fetchall():
        row = list(row)
        row[-2] = lib.str_time(row[-2])
        if row[-1] == 1:
            out = db.execute('SELECT timestamp, urea, creatinine, uric, albumin, bilirubin_direct, bilirubin_total, glucose, pt, asot, cbc, gpt, crp, hepatitis, esr FROM blood_out WHERE code=?', (row[0],)).fetchall()[0]
            out = list(out)
            out[0] = lib.str_time(out[0])
        else:
            out = [''] * 15
        row.extend(out)
        data.append(row)
    return json.dumps(data)
