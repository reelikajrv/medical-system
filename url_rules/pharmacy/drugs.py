from flask  import *

import json

def get():
   db = get_db('database')
   data = db.execute('SELECT drug, SUM(qty) FROM dispensed_drugs GROUP BY drug ORDER BY drug').fetchall()
   return json.dumps(data)
