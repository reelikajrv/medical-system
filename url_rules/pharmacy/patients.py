from flask  import *

import json

def post():
    try:
        db = get_db('database')
        action = request.form.get('action', 'insert')
        code = request.form['code'].strip().lower()
        diag = request.form['diag']
        refer_to = request.form['refer_to']
        refer_tel = request.form['refer_tel']

      #test = db.execute('SELECT * FROM patients WHERE code=?', (code,)).fetchall()
      #if not test:
      #   return '<span class="error">Invalid patient code</span>'
      #test = db.execute('SELECT * FROM patients WHERE code=? AND stage!=3', (code,)).fetchall()
      #if not test:
      #   return '<span class="error">Patient already left pharmacy</span>'   

        drugs = {}
        for key in request.form:
            if key.startswith('drugs-'):
                index = key.split('-')[1]
                d = request.form['drugs-' + index].strip().lower()
                q = request.form['qty-'  + index].strip()
                if not d:
                    continue
                if not q:
                    q = '1'
                q = int(q)
                drugs[d] = q

        if action == 'insert':
            db.execute('INSERT INTO pharmacy_out (code, diagnosis, drugs, date, timestamp, refer_to, refer_tel) VALUES (?, ?, ?, ?, ?, ?, ?)', (
                code,
                diag,
                json.dumps(drugs),
                lib.date(),
                lib.time(),
                refer_to,
                refer_tel
                ))
        elif action == 'update':
            db.execute('UPDATE pharmacy_out SET diagnosis=?, drugs=?, date=?, timestamp=?, refer_to=?, refer_tel=? WHERE code=?', (
                diag,
                json.dumps(drugs),
                lib.date(),
                lib.time(),
                refer_to,
                refer_tel,
                code
                ))
        for drug in drugs:         
            db.execute('INSERT INTO dispensed_drugs (drug, qty, date, timestamp) VALUES (?, ?, ?, ?)', (
                drug,
                drugs[drug],
                lib.date(),
                lib.time()
                ))
        db.execute('UPDATE patients SET stage=3 WHERE code=?', (code,))
        return '<span class="success">Done.</span>'

    except Exception as error:
        return '<span class="error">Server Error: %s</span>' %error
