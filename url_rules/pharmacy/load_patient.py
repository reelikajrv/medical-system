from flask import *

import json

def get():
    response = dict()
    code = request.args['code'].strip().lower();
    db = get_db('database')
    test = db.execute('SELECT * FROM patients WHERE code=?', (code,)).fetchall()
    if not test:
      response['message'] = '<span class="warning">Warning: Patient not in reception.</span>'
      return json.dumps(response)
    rst = db.execute('SELECT diagnosis, drugs FROM pharmacy_out WHERE code=?', (code,)).fetchall()
    if not rst:
        return ''
    else:
        drugs = []
        dsrc = json.loads(rst[0][1])
        for key in dsrc:
            drugs.append((key, dsrc[key]))
        response['diag'] = rst[0][0]
        response['drugs'] = drugs
        return json.dumps(response)
