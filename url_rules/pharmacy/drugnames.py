from flask  import *

import json

def get():
    names = []
    db = get_db('database')
    for name in db.execute('SELECT DISTINCT drug FROM dispensed_drugs ORDER BY drug').fetchall():
        names.extend(name)
    return json.dumps(names)
