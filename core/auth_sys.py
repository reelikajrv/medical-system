from flask  import session, flash, redirect, url_for

powers = { \
}

def require_auth(auth, function):
    def f(*args, **kwargs):
        logged_in   = session.get('loggedin', False)

        if 'login' in auth:
            if not logged_in:
                return redirect(url_for('/login'))

        return function(*args, **kwargs)
    return f
